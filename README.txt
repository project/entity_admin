
Entity Administration and history module
-----------------
by Julien Duteil, thedut

The Entity Administration and history module displays all entities on a unique
page, in a similar way that nodes can be administered on the 'admin/content'
page.

This new page will help to :
 - easly access to your datas or configurations.
 - monitor changes on you site, thanks to the history tag.
 - perform mass update or deletion operations.

For great usability, you can sort entities (Nodes, Users, Comments, Rules,
Views, Commerce Orders, etc..), filter them on entity type, bundle, author,
title, status. Links associated to entities are displayed ('edit', 'export',
'manage fields', and more).

This module provide history tag for all entities, similar as the one that
exists for nodes. Entities may by tagged as 'new' or 'changed' depending on the
history of the connected user.

For a full description of the module, visit the project page:
  http://drupal.org/project/entity_admin

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/entity_admin


-- GETTING START --

Go to 'admin/content/entities'.


-- REQUIREMENTS --

Entity API (https://www.drupal.org/project/entity).


-- INSTALLATION --

Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

Go to 'admin/config/administration/entity-admin'.
