<?php
/**
 * @file
 * Hooks provided by the entity_admin module.
 */

/**
 * Declare un object to display on the the entity admin page that is not an entity
 *
 * The object declaration look similary to the hook_entity_info() function, but
 * should reclare more informations.
 *
 * @return
 *   An array, the entity admin entity infos.
 *
 * @see hook_entity_admin_entity_info_alter()
 */
function hook_entity_admin_entity_info() {
  // From entity_admin_entity_admin_entity_info().
  $entity_info = array();
  if (module_exists('node')) {
    // Add node_type.
    $entity_info['node_type'] = array(
      'label' => t('Content type'),
      'plural label' => t('Content types'),
      'base table' => 'node_type',
      'bundle of' => 'node',
      'entity keys' => array(
        'id' => 'orig_type',
        'label' => 'name',
        'name' => 'type',
      ),
      'module' => 'node',
    );
  }

  // Add views
  if (module_exists('views')) {
    $entity_info['views'] = array(
      'label' => t('View'),
      'plural label' => t('Views'),
      'base table' => 'views_view',
      'entity keys' => array(
        'id' => 'vid',
        'label' => 'human_name',
        'name' => 'name',
        'status' => 'export_type',
      ),
      'exportable' => TRUE,
      // This is specific to the entity_admin module :
      'entity admin' => array(
        'load callback' => 'views_get_view',
        'path' => 'admin/structure/views/view',
        'properties' => array(
          // Status is available from loaded object, not from db (as 'disabled'
          // property).
          'status' => TRUE,
        ),
      ),
      'save callback' => 'views_save_view',
      'delete callback' => 'views_delete_view',
      'module' => 'views',
    );
  }
  return $entity_info;
}

/**
 * Alter the entity admin entity info which is a modified version of entity infos.
 *
 * Provide a new key to the entity_info array : the 'entity admin' key. Most
 * entries will be automatically set by the
 * entity_admin_entity_admin_entity_info_alter() function. The 'entity admin'
 * returns an array defined below :
 * - 'not entity', Boolean, TRUE if the object is not an entity, which means it
 *   is not returned by entity_get_info(). if TRUE, needs valid 'administer
 *   argument', 'load callback' keys and the entity info keys : 'save callback'
 *   and 'delete callback'.
 * - 'administer argument' : string, the permission for administer this object,
 *   as an example 'administer content types' for 'node_type'. Called with
 *   user_access().
 * - 'load callback': the function used to load an object which is not an entity,
 *   as an example 'node_type_load' for 'node_type'.
 * - 'external bundle': boolean . Comment and taxonomy_term entities don't store
 *    the bundle machine name in there own table but in an extra table. For
 *    comment entities the bundle is the type column from the node table, joined
 *    on nid columns and for taxonomy term entities the bundle is the
 *    machine_name column from the taxonomy_vocabulary table, joined on vid
 *    columns.
 * - 'config entity' : Boolean, if TRUE, entity type will be displayed in the
 *   'configuration' category on the entity type filter.
 * - 'delete multiple callback': a callback for deletion, for delete operations.
 * -  'path' : String, if entity_uri() fails, this value will be used for entity
 *    view path (ex : 'admin/structure/views/view' for 'views').
 * - 'properties': array of key-value. key is the property name, value is the
 *    column name that store this property. If the entity type does not store
 *    the propery, value is FALSE. If it stores the property but outside the
 *    entity base table, value is TRUE. Properties are defined in the
 *    entity_admin_properties() function. Entries are :
 *    - 'entity_type' : the 'Entity type', always TRUE.
 *    - 'bundle' : the bundle column name (ex : 'type' for 'node'),
 *    - 'title' :  the label column name (ex : 'name' for 'user'),
 *    - 'history' : Always TRUE thanks to the entity_admin_changed table that
 *      store changed date on entities that does not provide changed date
 *      storing.
 *    - 'author': the author column name (ex : 'uid' for 'node'),
 *    - 'status' : the status column name (ex : 'status' for 'node' but
 *      'active' for 'rules_config'). if the colonm name is 'disabled', the
 *      module will take care of the conversion.
 *    - 'changed': the changed timestamp column name (ex : 'changed' for 'node'),
 *    - 'created': the created timestamp column name (ex : 'created' for 'node'),
 *    - 'language': the language column name (ex : 'language' for 'node'),
 *    - 'entity_status': the export status column name (ex : 'status' for
 *      'rules_config').
 *
 * @param $entity_info
 *   The entity info array, keyed by entity name.
 *
 * @see hook_entity_info()
 * @see hook_entity_info_alter()
 */
function hook_entity_admin_entity_info_alter(&$entity_info) {
  // From entity_admin_entity_admin_entity_info_alter()
  if (isset($entity_info['user'])) {
    $entity_info['user']['entity keys']['label'] = 'name';
  }
}

/**
 * Add mass entity operations.
 *
 * This hook enables modules to inject custom operations into the mass
 * operations dropdown found at admin/content/entities, by associating a callback
 * function with the operation, which is called when the form is submitted. The
 * callback function receives one initial argument, which is an array of the
 * checked nodes.
 *
 * @return
 *   An array of operations. Each operation is an associative array that may
 *   contain the following key-value pairs:
 *   - label: (required) The label for the operation, displayed in the dropdown
 *     menu.
 *   - callback: (required) The function to call for the operation.
 *   - callback arguments: (optional) An array of additional arguments to pass
 *     to the callback function.
 */
function hook_entity_admin_operations() {
  // From entity_admin_entity_admin_operations().
  $operations = array();
  $operations['publish'] = array(
    'label' => t('Active selected content'),
    'callback' => 'entity_admin_mass_update',
    'callback arguments' => array('updates' => array('status' => NODE_PUBLISHED)),
  );
  return $operations;
}

/**
 * Provide additional header on the entity overview page.
 *
 * Modules that implements this hook must implement the
 * hook_customizable_entities_overview_page_values() hook to with the same keys
 * or provide a 'field' key : the $entity-><field key> will be displayed.
 * Additional columns will be displayed just before operation column.
 *
 * @param $entity_type
 *   the entity_type of a customizable entities.
 *
 * @return
 *   An array : first key is the column machine name, containing another array
 *   with the keys :
 *   - 'data' providing the human column name,
 *   -  optionally the 'field' key containing the column name in the schema
 *     table of this field. providing the 'field' key will enable sorting on its
 *     value.
 */
function hook_customizable_entities_overview_page_headers($entity_type) {
  // Example:
  $header = array();
  if ($entity_type == 'my_entity') {
    $header['created'] = array(
      'data' => t('Created'),
      'field' => 'created',
    );
    $header['id'] = array(
      'data' => t('ID'),
      'field' => 'id',
    );
  }
  return $header;
}

/**
 * Provide additional value per row on the entity overview page.
 *
 * Modules that implements this hook must declare first the header into the
 * hook_customizable_entities_overview_page_headers() hook.
 *
 * @param $entity_type
 *   the entity_type of a customizable entities.
 * @param $entity
 *   The customizable entities entity object.
 *
 * @return
 *   An array : first key is the column machine name, containing another array
 *   with the keys :
 *   - 'data' providing the human column name,
 *   -  optionally the 'field' key containing the column name in the schema
 *     table of this field. providing the 'field' key will enable sorting on its
 *     value.
 */
function hook_customizable_entities_overview_page_values($entity_type, $entity) {
  // Example from hook_customizable_entities_overview_page_header().
  $cells = array();
  if ($entity_type == 'my_entity') {
    $cells['created'] = format_date($entity->created, 'short');
    // The id header will be automatically filled.
  }
  return $cells;
}

/**
 * Provide additional links per row on the entity overview page (operation column).
 *
 * @param $entity_type
 *   the entity_type of a customizable entities.
 *
 * @return
 *   An array : first key is the link machine name, containing another array
 *   with the keys :
 *   - 'title' : string providing the human link name,
 *   -  'suffix_path' key : string corresponding to the path to append to entity
 *     view page path. As an example, for node entity with ID 1 the link in the
 *     example below will redirect to node/1/revisions.
 */
function hook_customizable_entities_overview_page_links($entity_type) {
  $links = array();
  if ($entity_type == 'my_entity') {
    // Note : this is an example. For a better integration, check if the entity
    // type is revisionnable and if revisions exists for this entity.
    $links['revisions'] = array(
      'title' => t('revisions'),
      'suffix_path' => 'revisions',
    );
  }
  return $links;
}