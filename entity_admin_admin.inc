<?php
/**
 * @file
 * Admininistration options the Entity administration module.
 */

/**
 * Form builder: Builds the entity administration options.
 *
 * @see entity_admin_admin_submit()
 *
 * @ingroup forms
 */
function entity_admin_admin($form, $form_state) {
  $form = array();

  $form['entity_admin'] = array(
    '#type' => 'markup',
    '#markup' => t('Options for the <a href="@url">entity administration page</a>', array('@url' => url('admin/content/entities'))),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $per_page_options = array();
  foreach (array(10, 20, 50, 75, 100) as $per_page) {
    $per_page_options[$per_page] = $per_page;
  }
  $form['entity_admin_entities_per_page'] = array(
    '#type' => 'select',
    '#title' => t('Number of entities to show per page'),
    '#options' => $per_page_options,
    '#default_value' => variable_get('entity_admin_entities_per_page', 50),
  );

  $default_types = variable_get('entity_admin_types', array());
  $entity_types = $default_types_options = array();
  foreach (entity_admin_entity_get_info() as $type => $info) {
    $entity_types[$type] = $info['label'];
    $default_types_options[$type] = (!empty($default_types) && in_array($type, $default_types)) ? $type : FALSE;
  }

  $form['entity_admin_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select the entity types that should be available'),
    '#description' => t('Optional, let them unchecked for all entity types.'),
    '#options' => $entity_types,
    '#default_value' => $default_types_options,
  );

  $current_headers = variable_get('entity_admin_headers', array());
  $default_headers = array();
  foreach (entity_admin_properties() as $name => $title) {
    $default_headers[$name] = (!empty($current_headers) && in_array($name, $current_headers)) ? $name : FALSE;
  }
  $form['entity_admin_headers'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select colomns to display'),
    '#description' => t('Optional, let them unchecked for all colomns.'),
    '#options' => entity_admin_properties(),
    '#default_value' => $default_headers,
  );

  $current_links = variable_get('entity_admin_links', array());
  $default_links = array();
  foreach (entity_admin_page_link_operations() as $link => $title) {
    $default_links[$link] = (!empty($current_links) && in_array($link, $current_links)) ? $link : FALSE;
  }
  $form['entity_admin_links'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select links to display per entities'),
    '#description' => t('Optional, let them unchecked for all links.'),
    '#options' => entity_admin_page_link_operations(),
    '#default_value' => $default_links,
  );


  return system_settings_form($form);
}

/**
 * Process entity_admin_admin form submissions.
 *
 * Save options into variables.
 *
 * @see entity_admin_admin()
 */
function entity_admin_admin_validate($form, &$form_state) {
  // executed before the submit function.
  // change array('node' => 0, 'user' => 1) to
  // array(0 => 'user').
  foreach (array('entity_admin_types', 'entity_admin_headers', 'entity_admin_links') as $variable) {
    $form_state['values'][$variable] = array_keys(array_filter($form_state['values'][$variable]));
  }
}
